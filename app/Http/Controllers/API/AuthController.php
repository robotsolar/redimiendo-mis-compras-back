<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(RegisterUserRequest $request)
    {
        
        $validData = $request->validated(); 
        $user = User::create([
            'name'      => $validData['name'],
            'username'  => $validData['username'],
            'phone'     => $validData['phone'],
            'birthday'  => $validData['birthday'],
            'password'  => bcrypt($validData['password'])
        ]);
        $token = $user->createToken('ngToken')->plainTextToken;
        $response = [
            'success'    => true,
            'payload'      => $user,
            'access_token'     => $token
        ];
        return response($response, 201);
    }

    
    public function login(Request $request) {
        $fields = $request->validate([
            'username' => 'required|string|max:255',
            'password' => 'required|string|max:255'
        ]);

        // Check username
        $user = User::where('username', $fields['username'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'success' => false,
                'message' => 'Bad Credentials'
            ], 200);
        }

        $token = $user->createToken('ngToken')->plainTextToken;

        $response = [
            'success'       => true,
            'payload'       => $user,
            'access_token'  => $token
        ];

        return response($response, 200);
    }

    public function logout(Request $request) {
        auth()->user()->tokens()->delete();
        $response = [
            'success' => true,
            'message' => 'Se ha cerrado sesion correctamente'
        ];
        return response($response, 200);

    }

    public function checkUsernameExists($username){
        $exists = User::where('username', $username)->exists();

        return response()->json([
            'exists' => $exists
        ], 200);
    }
}
