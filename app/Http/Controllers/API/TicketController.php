<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Http\Requests\StoreTicketRequest;
use App\Http\Resources\TicketResource;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $tickets = auth()->user()->tickets;
        $response = [
            'success'   => true,
            'payload'   => TicketResource::collection($tickets),
        ];
        
        return response($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicketRequest $request){

        $validData = $request->validated(); 
        //save image to folder public/images
        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $imageName);

        //create ticket
        $ticket = Ticket::create([
            'total'      => $validData['total'],
            'store_name'  => $validData['store_name'],
            'user_id'   => auth()->user()->id,
            'image'     => 'images/'.$imageName
        ]);
        //return response
        $response = [
            'success'    => true,
            'payload'      => $ticket,
        ];
        return response($response, 201);
    }

  
}
